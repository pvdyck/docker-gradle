#
# Gradle image based on Oracle JRE 8
#

FROM cwiesbaum/docker-maven
MAINTAINER 1science Devops Team <devops@1science.org>

ENV IVY_HOME /cache
ENV GRADLE_VERSION 2.14.1
ENV GRADLE_HOME /usr/local/gradle
ENV PATH ${PATH}:${GRADLE_HOME}/bin

# Install gradle
WORKDIR /usr/local

#install bash needed by gradle
RUN apk add --update bash && rm -rf /var/cache/apk/*

RUN wget  https://services.gradle.org/distributions/gradle-$GRADLE_VERSION-bin.zip && \
    unzip gradle-$GRADLE_VERSION-bin.zip && \
    rm -f gradle-$GRADLE_VERSION-bin.zip && \
    ln -s gradle-$GRADLE_VERSION gradle && \
    echo -ne "- with Gradle $GRADLE_VERSION\n" >> /root/.built

RUN apk update && apk add libstdc++ && rm -rf /var/cache/apk/*


WORKDIR /